import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  public isLogged : boolean;

  constructor(public afAuth: AngularFireAuth , private router: Router  ) { 
    this.afAuth.authState.subscribe( res => {
      this.isLogged = res !== null ? true : false ;
    })
  }

  ngOnInit() {
  }

  logout(){
    this.afAuth.auth.signOut().then(value => {
      this.router.navigate(['/login']);
    });
  }

}

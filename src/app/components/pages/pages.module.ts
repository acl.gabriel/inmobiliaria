import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PAGES_ROUTES } from './pages.routes';
import { ContenedorComponent } from './contenedor/contenedor.component';
import { DetailPropiedadComponent } from '../shared/detail-propiedad/detail-propiedad.component';
import { PropiedadesComponent } from './propiedades/propiedades.component';
import { FaqComponent } from './faq/faq.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { PageComponent } from './page.component';
import { HeaderComponent } from '../shared/header/header.component';
import { FooterComponent } from '../shared/footer/footer.component';
import { MapComponent } from '../shared/map/map.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PropiedadesService } from 'src/app/providers/propiedades.service';
import { FaqService } from 'src/app/providers/faq.service';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { AgmCoreModule } from '@agm/core';
import { NgxPaginationModule } from 'ngx-pagination';
import { UsersService } from '../../providers/users.service';

@NgModule({
  declarations: [
    ContenedorComponent,
    DetailPropiedadComponent,
    PropiedadesComponent,
    FaqComponent,
    EmpresaComponent,
    PageComponent,
    HeaderComponent,
    FooterComponent,
    MapComponent
  ],
  exports : [
    ContenedorComponent,
    DetailPropiedadComponent,
    PropiedadesComponent,
    FaqComponent,
    EmpresaComponent,
    PageComponent
  ],
  imports: [
    BrowserModule,
    PAGES_ROUTES,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireAuthGuardModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsKey
    })
  ],
  providers: [
    PropiedadesService,
    FaqService,
    UsersService
  ],
})
export class PagesModule { }

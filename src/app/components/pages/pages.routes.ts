import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './page.component';
import { ContenedorComponent } from './contenedor/contenedor.component';
import { FaqComponent } from './faq/faq.component';
import { EmpresaComponent } from './empresa/empresa.component';


const routes: Routes = [
  {   path: '', 
      component: PageComponent,
      children : [
        { path: '', component: ContenedorComponent },
        { path: 'faq', component: FaqComponent},
        { path: 'empresa', component: EmpresaComponent},
        { path: 'propiedad', component: ContenedorComponent },
        { path: 'propiedad/:id' , component: ContenedorComponent},
      ]
  },  
  ];

  export const PAGES_ROUTES = RouterModule.forChild(routes)
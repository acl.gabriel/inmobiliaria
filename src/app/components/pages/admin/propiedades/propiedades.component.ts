import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { PropiedadesService } from '../../../../providers/propiedades.service';

declare var $;

@Component({
  selector: 'app-propiedades',
  templateUrl: './propiedades.component.html',
  styleUrls: ['./propiedades.component.less']
})

export class PropiedadesComponent implements OnInit {
  @ViewChild('datatable') table : ElementRef;
  dataTable : any;
  public propiedades: any;
  constructor(public _PropService: PropiedadesService) {}

  ngOnInit() {
    this._PropService.onLoadPropiedades().subscribe((data)=> {
      this.propiedades = data;
      this.renderDataTable();     
    });
  
  }
  onDeletePropiedad(data){
    console.log(data)
  }
  renderDataTable(){
    setTimeout(() => {
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.dataTable();
    }, 100)
  }
}

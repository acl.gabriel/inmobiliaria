import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { FaqService } from '../../../../providers/faq.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

declare var $;

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.less']
})
export class FaqComponent implements OnInit {
  @ViewChild('modal') modal : ElementRef;

  public Faqs: Observable<any[]>;
  public formFAQs = new FormGroup({
    pregunta: new FormControl('', Validators.required),
    respuesta: new FormControl('', Validators.required)
  })
  constructor(public _FaqService : FaqService, private toastr: ToastrService) { }

  ngOnInit() {
    this.Faqs = this._FaqService.onLoadFaqs();
  }
  onSaveFaq(){
    this._FaqService.onSaveFaq(this.formFAQs.value, () => {
      this._onCloseModal();
      this.toastr.success('FAQ registrada exitosamente.','Registro de FAQ')
    });
  }
  onDeleteFaq(data){
    const self = this;
    this._FaqService.onDeleteFaq(data.id);
  }
  onUpdateFaq(){

  }
  _onCloseModal(){
    this.formFAQs.reset();
    let modal = $(this.modal.nativeElement);
    modal.modal('hide');
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ADMIN_ROUTES } from './admin.routes';
import { HeaderAdminComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ProfileComponent } from './profile/profile.component';
import { PropiedadesComponent } from './propiedades/propiedades.component';
import { UsuariosComponent } from './usuario/usuarios/usuarios.component';
import { MensajesComponent } from './mensajes/mensajes.component';
import { FaqComponent } from './faq/faq.component';
import { DataTablesModule } from 'angular-datatables';
import { PropiedadesService } from 'src/app/providers/propiedades.service';
import { FaqService } from '../../../providers/faq.service';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistroUsuarioComponent } from './usuario/registro-usuario/registro-usuario.component';

@NgModule({
  declarations: [
    DashboardComponent,
    SidebarComponent,
    HeaderAdminComponent,
    ProfileComponent,
    PropiedadesComponent,
    UsuariosComponent,
    MensajesComponent,
    FaqComponent,
    RegistroUsuarioComponent
  ],
  exports : [
    DashboardComponent,
    ProfileComponent,
    PropiedadesComponent,
    UsuariosComponent,
    MensajesComponent,
    FaqComponent,
    RegistroUsuarioComponent
  ],
  imports: [
    ADMIN_ROUTES,
    BrowserModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers : [
    PropiedadesService,
    FaqService
  ]
})
export class AdminModule { }

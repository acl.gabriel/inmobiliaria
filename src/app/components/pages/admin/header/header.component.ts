import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'admin-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderAdminComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth , private router: Router) { }

  ngOnInit() {
  }
  onLogout(){
    this.afAuth.auth.signOut().then(value => {
      this.router.navigate(['/']);
    });
  }
}

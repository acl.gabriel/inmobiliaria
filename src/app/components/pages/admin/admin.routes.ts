import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { ProfileComponent } from './profile/profile.component';
import { PropiedadesComponent } from './propiedades/propiedades.component';
import { UsuariosComponent } from './usuario/usuarios/usuarios.component';
import { MensajesComponent } from './mensajes/mensajes.component';
import { FaqComponent } from './faq/faq.component';
import { RegistroUsuarioComponent } from './usuario/registro-usuario/registro-usuario.component';
const routes: Routes = [
    {   path: 'admin', 
        component: DashboardComponent,
        canActivate: [AngularFireAuthGuard],
        children : [
          { path: 'propiedades', component: PropiedadesComponent},
          { path: 'usuarios', component: UsuariosComponent},
          { path: 'perfil', component: ProfileComponent },
          { path: 'mensajes', component: MensajesComponent},
          { path: 'faq', component: FaqComponent},    
          { path: 'new-usuario',component: RegistroUsuarioComponent}
        ]
    }  
  ];

  export const ADMIN_ROUTES = RouterModule.forChild(routes)
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UsersService } from '../../../../../providers/users.service';
import { Observable } from 'rxjs';
declare var $;

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.less']
})
export class UsuariosComponent implements OnInit {
  @ViewChild('datatable') table : ElementRef;
  dataTable : any;
  public usuarios: any;
  constructor(public _userService : UsersService) { }

  ngOnInit() {
    this._userService.onLoadUsers().subscribe((data)=> {
      this.usuarios = data;
      this.renderDataTable();     
    });
  }
  renderDataTable(){
    setTimeout(() => {
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.dataTable();
    }, 100)
  }

}

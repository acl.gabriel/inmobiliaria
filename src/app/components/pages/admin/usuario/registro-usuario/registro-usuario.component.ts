import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../../../../providers/users.service';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['./registro-usuario.component.less']
})
export class RegistroUsuarioComponent {
  public formRegisterUser = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
  })
  constructor(public _userService : UsersService) { }

  onSaveUser(){
    this._userService.onSaveUSer(this.formRegisterUser.value)
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';


//Librerias
import {AngularFireModule} from '@angular/fire'
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';


//Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';

//Modulos
import { AdminModule } from './components/pages/admin/admin.module';
import { PagesModule } from './components/pages/pages.module';
import { AuthenticationService } from './providers/authentication.service';


@NgModule({
  declarations: [
    AppComponent, 
    LoginComponent
  ],
  imports: [
    AdminModule,
    PagesModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireAuthGuardModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers:[
    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

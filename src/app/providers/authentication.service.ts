import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(public afAuth: AngularFireAuth) { }

  onLoginUser(data, successCallback, errorCallback) {
    this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password)
      .then(value => {
        successCallback()
      })
      .catch(err => {
        errorCallback(err)
      });
  }
  onSaveUser(data, successCallback, errorCallback){
    this.afAuth.auth.createUserWithEmailAndPassword(data.email, data.password)
    .then(data => {
      successCallback(data)
    })
    .catch(err => {
      errorCallback(err)
    });
  }

  onDeleteUser(Uid,successCallback,errorCallback){
    // this.afAuth.auth.getUser(Uid)
    // .then(data => {
    //   successCallback(data)
    // })
    // .catch(err => {
    //   errorCallback(err)
    // });
  }
}


import { Injectable, Output, EventEmitter } from '@angular/core'
import { map } from 'rxjs/operators';
import { Config } from './config';


export interface Item {
  barrio: string;
  calle: string;
  cuidad: string;
  cerca?: string;
  entre?: string,
  descripcion: string;
  dormitorios: string;
  estado: string;
  id_propiedad: number;
  id_tipo_moneda: string;
  image: string;
  latitud: string;
  nro: number;
  precio: number;
  publicar: number;
  tipo_operacion: string;
  tipo_propiedad: string;
  lat: number;
  lng: number;
}

@Injectable({
  providedIn: 'root'
})
export class PropiedadesService {

  public propiedades : any;

  @Output() change: EventEmitter<boolean> = new EventEmitter();


  constructor(private confService : Config) {
    this.confService.confServiceurl('propiedad');
  }
  onLoadPropiedades(){
    return this.confService.onLoadCollections();
  }

  onLoadDetailPropiedad(id){
    return this.confService.onLoadDocument(id);
  }

  onFilterPropiedades(Filter : any, OrderBy : string){
    this.propiedades = this.confService.onLoadFilterCollections(Filter, OrderBy);
    this.change.emit(this.propiedades);
  }
}

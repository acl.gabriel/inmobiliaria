import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })
export class Config {

    private itemsCollection: AngularFirestoreCollection;
    private itemDoc:AngularFirestoreDocument;
    public urlService:string;
    constructor( private db : AngularFirestore) {}
    
    confServiceurl(urlService:string){
        this.urlService = urlService;
        this.itemsCollection = this.db.collection(urlService);
    }
    onLoadCollections() {
        let data = this.itemsCollection.snapshotChanges().pipe(
            map(actions => actions.map(a => {
                const data = a.payload.doc.data()
                const id = a.payload.doc.id;
                return { id, ...data };
            }))
        );
        return data;
    }
    onLoadFilterCollections(Filters? : any, OrderBy? : string){

        let data = this.db.collection(this.urlService, ref => {
            let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
                for (let row in Filters) {
                    query = query.where(row , '==', Filters[row]) ;
                }
            if(OrderBy) query = query.orderBy(OrderBy, "asc")
            return query;
          }).snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data()
            const id = a.payload.doc.id;
            return { id, ...data };
          }))
        );
        return data;

    }
    onLoadDocument(id:string){
        let ids = `/${this.urlService}/${id}`;        
        this.itemDoc = this.db.doc(ids);
        let data = this.itemDoc.valueChanges();
        return data;
    }
    onSaveDocument(data : any, successCallback, errorCallback){
        this.itemsCollection.add(data).then(function() {
            successCallback()
        }).catch(function(error) {
            errorCallback()
        });;      
    }
    onSaveDocumentWithID(id,data,successCallback, errorCallback){
        this.itemsCollection.doc(id).set(data).then(function() {
            successCallback()
        }).catch(function(error) {
            errorCallback()
        });; 

    }
    onDeleteDocument(id:string, successCallback, errorCallback){
        this.itemsCollection.doc(id).delete().then(function() {
            successCallback()
        }).catch(function(error) {
            errorCallback()
        });
    }
}

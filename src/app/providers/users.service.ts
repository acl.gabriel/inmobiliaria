import { Injectable } from '@angular/core';
import { Config } from './config';
import { AuthenticationService } from './authentication.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


export interface Users {
  name: string;
  lastname: string;
  email: string;
  password: string;
  rol: string;
  image_profile: string;
}
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    public confService: Config, 
    public _AuthService: AuthenticationService,
    private toastr : ToastrService, 
    private router : Router
  ) {
    this.confService.confServiceurl('users');
  }
  onLoadUsers() {
    return this.confService.onLoadCollections();
  }
  onSaveUSer(data: Users) {
    const self = this;
    this._AuthService.onSaveUser(data,
      (dataUser) => { 
        let id = dataUser.user.uid;
        delete data.password;
        this.confService.onSaveDocumentWithID(id,data,
          () => {
            this.router.navigate(['/admin/usuarios']);
            self.toastr.success('Usuario creadoexitosamente.', 'Registro de Usuario')
          },
          (error) => {
            self.toastr.error('Ha ocurrido un error al registrar el usuario.', 'Error!')
          });
      },
      (error) => {
        self.toastr.error(error, 'Error!')
      });

  }
}

import { Injectable } from '@angular/core';
import { Config } from './config';
import { ToastrService } from 'ngx-toastr';


export interface Faqs {
  respuesta: string;
  pregunta: string;
}
@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor(private confService: Config, private toastr : ToastrService) {
    this.confService.confServiceurl('faq');
  }
  onLoadFaqs() {
    return this.confService.onLoadCollections();
  }
  onSaveFaq(data: Faqs, successCallback) {
    const self = this;
    this.confService.onSaveDocument(data, successCallback, () => {
      self.toastr.error('Ha ocurrido un error al registrar la FAQ.', 'Error!')
    });
  }
  onDeleteFaq(id) {
    const self = this;
    this.confService.onDeleteDocument(id, () => {
      self.toastr.success('FAQ eliminada exitosamente.', 'Eliminar FAQ')
    },
    () => {
        self.toastr.error('Ha ocurrido un error al eliminar la  FAQ.', 'Error!')
      });
  }
}
